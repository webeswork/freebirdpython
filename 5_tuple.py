print("Python oktatás - FREE BIRD Programozó Képző Társulat")
print("tuple, casting\n\n")

# tuple - rendezett, véges lista
vezetok = ('Kovács Béla', 'Magdi néni', 'Buksi kutya')
#print ('vezetok típusa:',  type(vezetok))

#print( 'Egyik vezető: ', vezetok[0])
#print( 'Egyik vezető: ', vezetok[10])

#vezetok[1]= 'Zoli bácsi'

tanulok = ('Teri nagymama', 'Zoli', 'Peti')

emberek = vezetok + tanulok

tanulok2 = [ 'Évi','Lali','Béla', 'Cili', 'Mici']

print ('tanulok2  típusa:',  type(tanulok2))
#print( 'tanulok 2 : ', tanulok2[10])

#emberek = vezetok + tanulok + tanulok2

emberek = list(emberek) + tanulok2


for ember in emberek:
    print('Ember: ', ember)

 


