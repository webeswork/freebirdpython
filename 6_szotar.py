print("Python oktatás - FREE BIRD Programozó Képző")
print("szótár\n\n")

# szótár  kulcs : elem
programozok = {'zolix':'Zoli', 'petix':'Peti'}

programozok['katix'] = 'Kati'
#del programozok['petix']

#Szótár items() metódusa!!:
#The method items() returns a list of dict's (key, value) tuple pairs

for kulcs, elem in programozok.items():
    print('Programozó: ', kulcs, ':', elem ) 


