print("Python oktatás - FREE BIRD Programozó Képző")
print("Arcade + Osztályok, Objektumok\n\n")

import arcade
import random


#Fej osztály
class Fej:
  def __init__(self, x, y, radius ):
   self.x= x
   self.y = y
   self.radius = radius

  def rajz(self):

    global arcade
    arcade.draw_circle_filled (self.x, self.y, self.radius-3, arcade.color.WHITE)
    arcade.draw_circle_outline(self.x, self.y, self.radius, arcade.color.BLACK, 5)
    arcade.draw_circle_filled (self.x -20, self.y+20, 10, arcade.color.BLACK)
    arcade.draw_circle_filled (self.x +20, self.y+20, 10, arcade.color.BLACK)
    arcade.draw_arc_outline(self.x , self.y-20, 40, 0, arcade.color.BLACK, 250, 290, 5)

# Képernyő méret
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Képernyő nyitás, méret, cím beállítása
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing Example")

# Képernyő háttér beállítás
arcade.set_background_color(arcade.color.WHITE)

# Renderelés kezdése
arcade.start_render()


#fej1 = Fej(500, 400, 100)
#fej1.rajz()

# veletlen elojel
#velEl =  ((-1)**random.randrange(2))

for fej in range(1,10):

    #velX = int(random.random() * 300)
    velX = random.randint(80, 700)
    velY = random.randint(80, 500)
    velR = random.randint(80, 100)
    #print ("x, y: ", velX, velY)
    fej = Fej(velX, velY, velR)
    fej.rajz()




# Finish drawing and display the result
arcade.finish_render()

# Keep the window open until the user hits the 'close' button
arcade.run()
