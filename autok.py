print("Python oktatás - FREE BIRD Programozó Képző")
print("Autók Osztályok, Objektumok\n\n")


class Auto():  # az autók osztálya

    def __init__(self, marka, szin, ar):

        self.marka = marka
        self.szin = szin
        self.ar = ar

    def aratvaltoztat(self):
        # szin felar szamítás
        felar = 1

        if self.szin == 'piros':
            felar = 1.135
        elif self.szin == 'kek':
            felar = 1.009
        elif self.szin == 'zold':
            felar = 0.8

        self.ar = round(felar * self.ar)

    def megjelenik(self):
        print('Márka:', self.marka, '| Szín:', self.szin, '| Ár:', self.ar)


autok_adat = (('opel', 'piros', 1000), ('opel', 'kek', 1000),
              ('opel', 'zold', 1000), ('opel', 'feher', 1000))

for marka, szin, alapar in autok_adat:
    auto_egyed = Auto(marka, szin, alapar)
    auto_egyed.aratvaltoztat()
    auto_egyed.megjelenik()
