print("Python oktatás - FREE BIRD Programozó Képző")
print("Chatbot játék \n\n")

import requests
import json
import random


# The main function that will grab a reply


def grab_reply(question):
    # Navigate to the Search Reddit Url
    r = requests.get('https://www.reddit.com/r/AskReddit/search.json?q=' +
                     question + '&sort=relevance&t=all', headers={'User-agent': 'Chrome'})

    answers = json.loads(r.text)  # Load the JSON file

    Children = answers["data"]["children"]

    ans_list = []

    for post in Children:
        if post["data"]["num_comments"] >= 5:  # Greater then 5 or equal  comments
            ans_list.append(post["data"]["url"])

    # If no results are found return "I have no idea"
    if len(ans_list) == 0:
        return "I have no idea"

    # Pick A Random Post
    # Grab Random Comment Url and Append .json to end
    comment_url = ans_list[random.randint(
        0, len(ans_list) - 1)] + '.json?sort=top'

    # Navigate to the Comments
    r = requests.get(comment_url, headers={'User-agent': 'Chrome'})
    reply = json.loads(r.text)

    Children = reply[1]['data']['children']

    reply_list = []

    for post in Children:

        if("body" in post["data"]):
          foo =post["data"]["body"]
          #baz = foo.encode("utf-8")
          baz = foo
          #print (baz)
          # Add Comments to the List
          reply_list.append(baz)

    if len(reply_list) == 0:
        return "I have no clue"

    # Return a Random Comment
    return reply_list[random.randint(0, len(reply_list) - 1)]


# Main Loop, Always ask for a question
while (1):
    q = input("Ask me! [key 'q' - quit ]: ")
    if(q =='q'):
        break
    else:
       q = q.replace(" ", "+")  # Replace Spaces with + for URL encoding
       print(grab_reply(q))  # Grab and Print the Reply
    
    
