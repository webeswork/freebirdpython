from django.db import models

# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='images/')
    desc = models.TextField()
    price = models.CharField(max_length=20)
    pub_date = models.DateField()


        #olvasható címek az admin oldalon
    def __str__(self):
        return self.title

    def pub_date_mod(self):
       return self.pub_date.strftime('%Y.%m.%d.')
