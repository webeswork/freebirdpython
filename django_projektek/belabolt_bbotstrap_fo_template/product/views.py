from django.shortcuts import render
from django.shortcuts import get_object_or_404

from .models import Product


def home(request):
    products = Product.objects
    return render (request, 'home.html', {'products':products})


def detail(request, product_id):
    detail_product = get_object_or_404(Product, pk=product_id)
    return render(request, 'detail.html', {'product': detail_product})
