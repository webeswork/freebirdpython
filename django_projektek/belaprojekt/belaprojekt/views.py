from django.http import HttpResponse
from django.shortcuts import render




def home(request):
    return render( request, 'home.html', { 'alma': 'Starking alma jó. Szeretem.'})



def eredmeny(request):
    szoveg = request.GET['szoveg']
    betuk_szama = len(szoveg)
    #print( 'Teszteles: ' + szoveg)
    return render( request, 'eredmeny.html' , { 'szoveg' : szoveg , 'betuk_szama' : betuk_szama})
