print("Python oktatás - FREE BIRD Programozó Képző")
print("Ablak készítés \n\n")
import pygame
import sys
pygame.init()

szelesseg = 300
hossz = 300
ablakMeret= (szelesseg,hossz)

ablak = pygame.display.set_mode(ablakMeret)

pygame.display.set_caption("Ablak készítés")

vege = False
while vege == False:
    # esemenyeket figyeli,
    #osszes esemeny behivasa
    for event in pygame.event.get():
        #egyezteti, hogy melyik hajtodott vegre
        if event.type == pygame.QUIT:
            vege = True
            pygame.quit()
            sys.exit()

    #pygame játék frissitese
    pygame.display.flip()


#pygame.quit()
