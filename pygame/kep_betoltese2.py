print("Python oktatás - FREE BIRD Programozó Képző")
print("Kép betöltése  - Macska és az egér \n\n")
import pygame
import sys
pygame.init()

szelesseg = 300
hossz = 300
ablakMeret= (szelesseg,hossz)

ablak = pygame.display.set_mode(ablakMeret)

pygame.display.set_caption("Kép betöltése ")


kep = pygame.image.load("macska.jpg").convert()
kep = pygame.transform.scale(kep, (szelesseg,hossz ))
ablak.blit(kep, (0,0))

eger = pygame.image.load("eger.png").convert()


arany = 0.5
egerSzelesseg = eger.get_rect().width
egerMagassag = eger.get_rect().height
eger = pygame.transform.scale(eger, (int(egerSzelesseg*arany),int(egerMagassag*arany)))


eger = pygame.transform.rotate(eger, 90)

ablak.blit(eger, (50,50))

vege = False
while vege == False:
    # esemenyeket figyeli,
    #osszes esemeny behivasa
    for event in pygame.event.get():
        #egyezteti, hogy melyik hajtodott vegre
        if event.type == pygame.QUIT:
            vege = True
            pygame.quit()
            sys.exit()

    #pygame játék frissitese
    pygame.display.flip()


#pygame.quit()
