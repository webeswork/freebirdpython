print("Python oktatás - FREE BIRD Programozó Képző")

cim = "Kép vágása"
print(cim + "\n\n")
import pygame
import sys
pygame.init()

def vagas(ujSzel, ujMag, vagasX, vagasY,  kep):
    #ujFelulet = pygame.Surface((ujSzel, ujMag) )
    ujFelulet = pygame.Surface((ujSzel, ujMag),pygame.SRCALPHA, 32 )
    ujFelulet.blit(kep, (0,0 ), ( vagasX, vagasY, ujSzel, ujMag))
    return ujFelulet



szelesseg = 600
hossz = 400
ablakMeret= (szelesseg,hossz)

ablak = pygame.display.set_mode(ablakMeret)

pygame.display.set_caption(cim)


kep = pygame.image.load("kutya.jpg").convert()
kep = pygame.transform.scale(kep, (szelesseg,hossz ))
ablak.blit(kep, (0,0))

# 260x238 px
macska = pygame.image.load("macska.jpg").convert()
macska = vagas(100, 100 , 140, 10,macska)
ablak.blit(macska , (20,20))

#147x150px
eger = pygame.image.load("eger.png").convert()
eger = vagas(100, 100 , 50, 50,eger)
ablak.blit(eger , (400,200))



vege = False
while vege == False:
    # esemenyeket figyeli,
    #osszes esemeny behivasa
    for event in pygame.event.get():
        #egyezteti, hogy melyik hajtodott vegre
        if event.type == pygame.QUIT:
            vege = True
            pygame.quit()
            sys.exit()

    #pygame játék frissitese
    pygame.display.flip()


#pygame.quit()
