print("Python oktatás - FREE BIRD Programozó Képző")
print("Selenium - Belépés a Facebookra, egy belső oldal tartalmának kiolvasása\n\n")


from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait

import getpass

email_konzolrol = input('Kérlek, írd be az email címed: ')

jelszo_konzolrol = getpass.getpass('Kérlek, írd be a jelszavad: ')


# https://www.seleniumhq.org/projects/webdriver/
#browser = webdriver.Firefox()
browser = webdriver.Chrome("C:\Appok\Python\Selenium\drivers\chromedriver.exe")
#browser = webdriver.Ie()

browser.get("https://www.facebook.com/")

username = browser.find_element_by_id("email")
password = browser.find_element_by_id("pass")
submit = browser.find_element_by_id("loginbutton")


username.send_keys(email_konzolrol)
password.send_keys(jelszo_konzolrol)


submit.click()


wait = WebDriverWait(browser,5)

login_page = "https://www.facebook.com/"

try:
        page_loaded = wait.until_not(
            lambda browser: browser.current_url == login_page
        )

except TimeoutException:
    print ("Loading timeout expired")
except:
    print ("Error")

browser.get("https://www.facebook.com/groups/python.programozok/")




#elements = browser.find_elements_by_xpath("//li")
elements = browser.find_elements_by_xpath("//p")
#elements = browser.find_elements_by_class_name("profileLink")


#print( elements)

for foo in elements:
    print(foo.text)
