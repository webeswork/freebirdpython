print("Python oktatás - FREE BIRD Programozó Képző\n\n")
print("Józsi kedvelte függveny\n\n")


def kedvelte(names):

    elvalaszto = " és "
    elvalaszto2 = ", "

    if len(names) == 0:
        return "Még senki sem kedvelte ezt a bejegyzést."
    elif len(names) == 1:
        return ''.join(names) + " kedvelte ezt a bejegyzést."
    elif len(names) == 2:
        baz = elvalaszto.join(names)
        return baz + " kedvelte ezt a bejegyzést."
    elif len(names) == 3:
        kezdo_lista = names[0:2]
        kezdo = elvalaszto2.join(kezdo_lista)
        maradek_lista = names[2:3]
        maradek = ''.join(maradek_lista)
        return kezdo + " és " + maradek + " kedvelte ezt a bejegyzést."
    elif len(names) > 3:
        kezdo_lista = names[0:2]
        maradek_lista = names[2:]
        kezdo = elvalaszto2.join(kezdo_lista)
        return kezdo + " és további " + str(len(maradek_lista)) + " felhasználó kedvelte ezt a bejegyzést."




print(kedvelte([]))
print(kedvelte(['Peti']))
print(kedvelte(['Juli', 'Gabi']))
print(kedvelte(['Peti', 'Juli', 'Gabi']))
print(kedvelte(['Peti', 'Juli', 'Gabi','Sanyi', 'Éva']))
print(kedvelte(['Peti', 'Juli', 'Gabi','Sanyi', 'Éva', 'Zoli','Eszter']))
print(kedvelte(['Peti', 'Juli', 'Gabi','Sanyi', 'Éva', 'Zoli','Eszter', 'Viki','Miki']))
